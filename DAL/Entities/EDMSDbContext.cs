﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class EDMSDbContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AssignedWork> AssignedWorks { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserPosition> UserPositions { get; set; }

        public EDMSDbContext(DbContextOptions<EDMSDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();   // создаем базу данных при первом обращении
        }
    }
}
