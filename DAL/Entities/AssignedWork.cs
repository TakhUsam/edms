﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class AssignedWork : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public TypeWork TypeWork { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateDue { get; set; }
        public string Url { get; set; }
        public int UserSenderId { get; set; }
        public User UserSender { get; set; }
        public int UserExecuterId { get; set; }
        public User UserExcuter { get; set; }
    }

    public enum TypeWork
    {
        Reguest = 0,
        Oblige = 1
    }
}
