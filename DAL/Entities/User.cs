﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class User : BaseEntity
    {
        public string FirstName { get; set; }
        public string Name { get; set; }
        public string MiddleName { get; set; }
        [EmailAddress]
        public string Emeil { get; set; }
    }
}

