﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class UserPosition : BaseEntity
    {
        public int UserId { get; set; }
        public int PositionId { get; set; }

        public Position Position { get; set; }
        public User User { get; set; }
    }
}
