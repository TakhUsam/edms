﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Account : BaseEntity
    {
        [Required(ErrorMessage = "Логин обязательный")]
        public string Login { get; set; }
        [MaxLength(20)]
        public string Password { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

    }
}
