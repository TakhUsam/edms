﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// Base class for Entity
    /// </summary>
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
