﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace DTO.Validate
{
    public static class ValidateDTO
    {
        public static bool IsValid(this AssignedWorkDTO aWork)
        {
            if (string.IsNullOrEmpty(aWork.Title) ||
               string.IsNullOrEmpty(aWork.Description) ||
               string.IsNullOrEmpty(aWork.Url) ||
               !aWork.DateCreate.HasValue ||
               !aWork.DateDue.HasValue ||
               !aWork.UserExecuterId.HasValue ||
               !aWork.UserSenderId.HasValue)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
