﻿using AutoMapper;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTO.Mapper
{
    public class Mapper : Profile
    {
        public Mapper()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<AssignedWork, AssignedWorkDTO>();
            CreateMap<AssignedWorkDTO, AssignedWork> ();
        }
    }
}
