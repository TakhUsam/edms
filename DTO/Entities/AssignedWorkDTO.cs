﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class AssignedWorkDTO : BaseEntityDTO
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public TypeWork TypeWork { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateDue { get; set; }
        public string Url { get; set; }

        public int? UserSenderId { get; set; }
        public UserDTO UserSender { get; set; }
        public int? UserExecuterId { get; set; }
        public UserDTO UserExcuter { get; set; }

        public bool IsValid()
        {
            if (string.IsNullOrEmpty(this.Title) ||
               string.IsNullOrEmpty(this.Description) ||
               string.IsNullOrEmpty(this.Url) ||
               !this.DateCreate.HasValue ||
               !this.DateDue.HasValue)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }


    public enum TypeWork
    {
        Reguest = 0,
        Oblige = 1
    }
}
