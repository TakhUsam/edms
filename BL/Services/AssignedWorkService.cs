﻿using DAL.Entities;
using DTO.Validate;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Services
{
    class AssignedWorkService
    {
        private EDMSDbContext DBContext;

        public AssignedWorkService(EDMSDbContext dbContext)
        {
            DBContext = dbContext;
        }

        public async void AssignedWork(AssignedWorkDTO aWork)
        {
            if (!aWork.IsValid())
            {
                throw new Exception("Заполните все поля");
            }

        }

    }
}
