﻿using DAL.Entities;
using Microsoft.AspNetCore.Mvc;

namespace EDMS.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class MainController : ControllerBase
    {
        private EDMSDbContext DbContext;

        public MainController(EDMSDbContext _EDMSDbContext)
        {
            DbContext = _EDMSDbContext;
        }

        [HttpGet]
        public string Info()
        {
            return "This project was created by the group PI-2017";
        }
    }
}
